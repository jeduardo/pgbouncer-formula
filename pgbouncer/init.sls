pgbouncer:
  pkg.installed:
    - name: pgbouncer

/etc/pgbouncer/pgbouncer.ini:
  file.managed:
    - source: salt://{{ slspath }}/files/pgbouncer.ini.j2
    - mode: 600
    - template: jinja
    - require:
      - pkg: pgbouncer

/etc/pgbouncer/userlist.txt:
  file.managed:
    - source: salt://{{ slspath }}/files/userlist.txt.j2
    - mode: 600
    - template: jinja
    - require:
      - pkg: pgbouncer

/etc/logrotate.d/postgresql-common:
  file.managed:
    - source: salt://{{ slspath }}/files/postgresql-common
    - mode: 644
    - require:
      - pkg: pgbouncer

pgbouncer-service:
  service.running:
    - name: pgbouncer
    - enable: True
    - reload: True
    - on_changes:
      - file: /etc/pgbouncer/pgbouncer.ini
    - require:
      - pkg: pgbouncer
